import { BigNumber } from "@ethersproject/bignumber";
import { expect } from "chai";
import { ethers, network } from "hardhat";

describe("UNISWAP FORK", function () {

  it("Deploy contracts", async function () {
    const WETH = await ethers.getContractFactory("WETH");
    const FDAI = await ethers.getContractFactory("FDAI");
    const FUSDC = await ethers.getContractFactory("FUSDC");
    const FUSDT = await ethers.getContractFactory("FUSDT");
    const BST = await ethers.getContractFactory("BST");
    const StkBST = await ethers.getContractFactory("StkBST");
    const MasterChef = await ethers.getContractFactory("MasterChef");
    const UniswapV2Factory = await ethers.getContractFactory("UniswapV2Factory");
    const UniswapV2Router02 = await ethers.getContractFactory("UniswapV2Router02");
    const UniswapV2Pair = await ethers.getContractFactory("UniswapV2Pair");


    const signers = await ethers.getSigners()
    const weth = await WETH.connect(signers[0]).deploy();
    const fdai = await FDAI.connect(signers[0]).deploy();
    const fusdc = await FUSDC.connect(signers[0]).deploy();
    const fusdt = await FUSDT.connect(signers[0]).deploy();
    const bst = await BST.connect(signers[0]).deploy();
    const stkbst = await StkBST.connect(signers[0]).deploy(bst.address);
    const masterchef = await MasterChef.connect(signers[0]).deploy(bst.address, signers[1].address, BigNumber.from("1000000000000000000"), BigNumber.from("10"), BigNumber.from("10000000000000"));

    await bst.connect(signers[0]).setMinter(masterchef.address, stkbst.address)

    const factory = await UniswapV2Factory.connect(signers[0]).deploy(signers[0].address);
    const router = await UniswapV2Router02.connect(signers[0]).deploy(factory.address, weth.address);

    await weth.connect(signers[0]).deposit({
      value: ethers.utils.parseEther("15.0")
    })

    await fdai.connect(signers[0]).mint(signers[0].address, ethers.utils.parseEther("5000.0"))
    await fusdc.connect(signers[0]).mint(signers[0].address, ethers.utils.parseEther("5000.0"))
    await fusdt.connect(signers[0]).mint(signers[0].address, ethers.utils.parseEther("5000.0"))

    await factory.createPair(weth.address, fdai.address)
    await factory.createPair(weth.address, fusdt.address)
    await factory.createPair(weth.address, fusdc.address)

    const fdaiPairAddr = await factory.getPair(weth.address, fdai.address)
    const fusdcPairAddr = await factory.getPair(weth.address, fusdc.address)
    const fusdtPairAddr = await factory.getPair(weth.address, fusdt.address)
    const fdaiPair = UniswapV2Pair.attach(fdaiPairAddr)
    const fusdcPair = UniswapV2Pair.attach(fusdcPairAddr)
    const fusdtPair = UniswapV2Pair.attach(fusdtPairAddr)
    console.log("fDAI Pair address: ", fdaiPairAddr)
    console.log("fUSDC Pair address: ", fusdcPairAddr)
    console.log("fUSDT Pair address: ", fusdtPairAddr)

    await weth.connect(signers[0]).transfer(fdaiPair.address, ethers.utils.parseEther("5.0"))
    await fdai.connect(signers[0]).transfer(fdaiPair.address, ethers.utils.parseEther("5000.0"))
    await fdaiPair.mint(signers[0].address)

    await weth.connect(signers[0]).transfer(fusdcPair.address, ethers.utils.parseEther("5.0"))
    await fusdc.connect(signers[0]).transfer(fusdcPair.address, ethers.utils.parseEther("3000.0"))
    await fusdcPair.mint(signers[0].address)

    await weth.connect(signers[0]).transfer(fusdtPair.address, ethers.utils.parseEther("5.0"))
    await fusdt.connect(signers[0]).transfer(fusdtPair.address, ethers.utils.parseEther("3000.0"))
    await fusdtPair.mint(signers[0].address)

    const fdaiLpTokenAmount = await fdaiPair.balanceOf(signers[0].address)
    const fusdcLpTokenAmount = await fusdcPair.balanceOf(signers[0].address)
    const fusdtLpTokenAmount = await fusdtPair.balanceOf(signers[0].address)

    console.log("fDAI/WETH LP Token received: ", fdaiLpTokenAmount.div(BigNumber.from("1000000000000000000")).toString())
    console.log("fUSDC/WETH LP Token received: ", fusdcLpTokenAmount.div(BigNumber.from("1000000000000000000")).toString())
    console.log("fUSDT/WETH LP Token received: ", fusdtLpTokenAmount.div(BigNumber.from("1000000000000000000")).toString())

    await masterchef.connect(signers[0]).add(BigNumber.from("50"), fdaiPair.address, false)
    await masterchef.connect(signers[0]).add(BigNumber.from("30"), fusdcPair.address, false)
    await masterchef.connect(signers[0]).add(BigNumber.from("20"), fusdtPair.address, false)

    await fdaiPair.connect(signers[0]).approve(masterchef.address, fdaiLpTokenAmount)
    await fusdcPair.connect(signers[0]).approve(masterchef.address, fusdcLpTokenAmount)
    await fusdtPair.connect(signers[0]).approve(masterchef.address, fusdtLpTokenAmount)

    let poolInfo0 = await masterchef.poolInfo(BigNumber.from("0"))
    let poolInfo1 = await masterchef.poolInfo(BigNumber.from("1"))
    let poolInfo2 = await masterchef.poolInfo(BigNumber.from("2"))

    await masterchef.deposit(BigNumber.from("0"), fdaiLpTokenAmount)
    await masterchef.deposit(BigNumber.from("1"), fusdcLpTokenAmount)
    await masterchef.deposit(BigNumber.from("2"), fusdtLpTokenAmount)

    for (let i = 0; i < 100; i++) {
      await network.provider.request({
        method: "evm_mine",
        params: [],
      });
    }

    let pendingBst0 = await masterchef.connect(signers[0]).pendingBst(0, signers[0].address)
    let pendingBst1 = await masterchef.connect(signers[0]).pendingBst(1, signers[0].address)
    let pendingBst2 = await masterchef.connect(signers[0]).pendingBst(2, signers[0].address)
    console.log("Pending BST after 100 block on pid 0: ", pendingBst0.div(BigNumber.from("1000000000000000000")).toString())
    console.log("Pending BST after 100 block on pid 1: ", pendingBst1.div(BigNumber.from("1000000000000000000")).toString())
    console.log("Pending BST after 100 block on pid 2: ", pendingBst2.div(BigNumber.from("1000000000000000000")).toString())

    await masterchef.connect(signers[0]).withdraw(0, BigNumber.from(0))
    await masterchef.connect(signers[0]).withdraw(1, BigNumber.from(0))
    await masterchef.connect(signers[0]).withdraw(2, BigNumber.from(0))

    let bstBalance = await bst.balanceOf(signers[0].address);
    console.log("Current BST Balance: ", bstBalance.div(BigNumber.from("1000000000000000000")).toString())

    await bst.connect(signers[0]).approve(stkbst.address, bstBalance);
    await stkbst.connect(signers[0]).depositAll()

    let stkbstBalance = await stkbst.balanceOf(signers[0].address);
    console.log("Current StkBST: ", stkbstBalance.div(BigNumber.from("1000000000000000000")).toString())

    let stkbstUnderlyingBalanceBefore = await stkbst.underlyingBalanceOf(signers[0].address);
    console.log("Current BST underlying balance before: ", stkbstUnderlyingBalanceBefore.div(BigNumber.from("1000000000000000000")).toString())

    await stkbst.connect(signers[0]).distribution();
    let stkbstUnderlyingBalanceAfter= await stkbst.underlyingBalanceOf(signers[0].address);
    console.log("Current BST underlying balance after: ", stkbstUnderlyingBalanceAfter.div(BigNumber.from("1000000000000000000")).toString())

    await stkbst.connect(signers[0]).withdrawAll()
    
    stkbstBalance = await stkbst.balanceOf(signers[0].address);
    console.log("Current StkBST: ", stkbstBalance.div(BigNumber.from("1000000000000000000")).toString())
    bstBalance = await bst.balanceOf(signers[0].address);
    console.log("Current BST: ", bstBalance.div(BigNumber.from("1000000000000000000")).toString())
    
    let usdcBalanceBefore = await fusdc.balanceOf(signers[0].address)
    let usdtBalanceBefore = await fusdt.balanceOf(signers[0].address)

    console.log("Before swap USDC: ", usdcBalanceBefore.div(BigNumber.from("1000000000000000000")).toString())
    console.log("Before swap USDT: ", usdtBalanceBefore.div(BigNumber.from("1000000000000000000")).toString())

    let path = [fusdc.address, weth.address, fusdt.address]

    await fusdc.approve(router.address, BigNumber.from("1000000000000000000"))
    await router.swapExactTokensForTokens(BigNumber.from("1000000000000000000"), BigNumber.from(0), path, signers[0].address, BigNumber.from("100000000000000000"))
    
    let usdcBalanceAfter = await fusdc.balanceOf(signers[0].address)
    let usdtBalanceAfter = await fusdt.balanceOf(signers[0].address)

    console.log("After swap USDC: ", usdcBalanceAfter.toString())
    console.log("After swap USDT: ", usdtBalanceAfter.toString())


    
  });


});
