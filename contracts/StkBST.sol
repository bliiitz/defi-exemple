//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "./BST.sol";

contract StkBST is ERC20 {

    BST bst;
    uint256 lastDistribution;
    uint256 public totalStacked;
    
    constructor(address _BST) ERC20("Stacked BST", "StkBST") {
        bst = BST(_BST);
    }

    function balance() public view returns (uint256) {
        return bst.balanceOf(address(this));
    }

    function underlyingBalanceOf(address _addr) public view returns (uint256) {
        return balance() * balanceOf(_addr) / totalSupply();
    }
    
    function deposit(uint256 _amount) public {
        uint256 _pool = balance();
        uint256 _before = bst.balanceOf(address(this));
        bst.transferFrom(msg.sender, address(this), _amount);
        uint256 _after = bst.balanceOf(address(this));
        _amount = _after - _before;
        uint256 shares = 0;
        if (totalSupply() == 0) {
            shares = _amount;
        } else {
            shares = _amount * totalSupply() / _pool;
        }
        _mint(msg.sender, shares);
    }

    function depositAll() public {
        deposit(
            bst.balanceOf(msg.sender)
        );
    }

    function withdraw(uint256 _shares) public {
        uint256 r = balance() * _shares/ totalSupply();
        _burn(msg.sender, _shares);
        bst.transfer(msg.sender, r);
    }

    function withdrawAll() public {
        withdraw(balanceOf(msg.sender));
    }

    function distribution() public {
        require(lastDistribution < block.timestamp + 3600);
        lastDistribution = block.timestamp;
        bst.mint(address(this), 1000 * 10**18);
    }
}