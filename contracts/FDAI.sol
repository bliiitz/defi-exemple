//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract FDAI is ERC20 {
    constructor() ERC20("Fake DAI", "FDAI") {
    }

    function mint(address _address, uint256 _amount) public {
        _mint(_address, _amount);
    }
}
