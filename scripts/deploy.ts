// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
import { ethers } from "hardhat";

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // We get the contract to deploy
  const WETH = await ethers.getContractFactory("WETH");
  const UniswapV2Factory = await ethers.getContractFactory("UniswapV2Factory");
  const UniswapV2Router02 = await ethers.getContractFactory("UniswapV2Router02");

  const signers = await ethers.getSigners()

  const weth = await WETH.deploy();
  const factory = await UniswapV2Factory.deploy(signers[0].address);
  const router = await UniswapV2Router02.deploy(factory.address, weth.address);

  await weth.deployed();
  await factory.deployed();
  await router.deployed();

  console.log("Greeter deployed to:", greeter.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
